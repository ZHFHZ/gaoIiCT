<!DOCTYPE html>
<?php
    require ('../../Config.php');
    $conn = new mysqli($MySqlHost,$MySqlUser,$MySqlPwd,$MySqlDatabaseName);
    $res = mysqli_query($conn,"SELECT COUNT(*) FROM user_users");
    $student_count = $title_count = mysqli_fetch_row($res)[0]; //查询学生数
    $res = mysqli_query($conn,"SELECT COUNT(*) FROM admin_users");
    $admin_count = $title_count = mysqli_fetch_row($res)[0]; //查询管理员数
    $res = mysqli_query($conn,"SELECT COUNT(*) FROM other");
    $title_count = mysqli_fetch_row($res)[0]; //查询题目号
    $res = mysqli_query($conn,"SELECT finish_question_num From information");//做题数
    $finish_question_num = mysqli_fetch_row($res)[0];
    $res = mysqli_query($conn,"SELECT error_question_num From information");//错题数
    $error_question_num = mysqli_fetch_row($res)[0];
    $admin_user_name = $_COOKIE['User_Name'];
    mysqli_free_result($res); //释放内存
    mysqli_close($conn);
?>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>欢迎页面-X-admin2.2</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="./css/font.css">
        <link rel="stylesheet" href="./css/xadmin.css">
        <script src="lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="./js/xadmin.js"></script>
        <script src="js/jquery.min.js"></script>
        <script src="../API/Cookie.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script>
            window.onload = function () {
                $.post("../API/Select_Admin.php",{
                    "User_Name":getCookie("User_Name"),
                    "Pass_Word":getCookie("User_Id")
                },function (data) {
                    if(data != "Error"){

                    }else {
                        alert("请先登录");
                        window.location.href = '../';
                    }
                })

            }
        </script>
    </head>
    <body>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body ">
                            <blockquote class="layui-elem-quote">欢迎管理员：
                                <span class="x-red"><?php echo $admin_user_name ?></span>  当前时间:<?php  echo date('Y-m-d h:i:s', time());  ?>
                            </blockquote>
                        </div>
                    </div>
                </div>
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-header">数据统计</div>
                        <div class="layui-card-body ">
                            <ul class="layui-row layui-col-space10 layui-this x-admin-carousel x-admin-backlog">
                                <li class="layui-col-md2 layui-col-xs6">
                                    <a href="javascript:;" class="x-admin-backlog-body">
                                        <h3>数据库题目数</h3>
                                        <p>
                                            <cite><?php echo $title_count ?></cite></p>
                                    </a>
                                </li>
                                <li class="layui-col-md2 layui-col-xs6">
                                    <a href="javascript:;" class="x-admin-backlog-body">
                                        <h3>学生用户数</h3>
                                        <p>
                                            <cite><?php echo $student_count ?></cite></p>
                                    </a>
                                </li>
                                <li class="layui-col-md2 layui-col-xs6">
                                    <a href="javascript:;" class="x-admin-backlog-body">
                                        <h3>管理员用户数</h3>
                                        <p>
                                            <cite><?php echo $admin_count ?></cite></p>
                                    </a>
                                </li>
                                <li class="layui-col-md2 layui-col-xs6">
                                    <a href="javascript:;" class="x-admin-backlog-body">
                                        <h3>商品数</h3>
                                        <p>
                                            <cite>67</cite></p>
                                    </a>
                                </li>
                                <li class="layui-col-md2 layui-col-xs6">
                                    <a href="javascript:;" class="x-admin-backlog-body">
                                        <h3>做题数</h3>
                                        <p>
                                            <cite><?php echo $finish_question_num ?></cite></p>
                                    </a>
                                </li>
                                <li class="layui-col-md2 layui-col-xs6 ">
                                    <a href="javascript:;" class="x-admin-backlog-body">
                                        <h3>错题数</h3>
                                        <p>
                                            <cite><?php echo $error_question_num ?></cite></p>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="layui-col-sm6 layui-col-md3">
                    <div class="layui-card">
                        <div class="layui-card-header">正确率
                            <span class="layui-badge layui-bg-cyan layuiadmin-badge">百分比</span></div>
                        <div class="layui-card-body  ">
                            <p class="layuiadmin-big-font"><?php echo   (1 - ($error_question_num / $finish_question_num)) * 100 . "%"  ?></p>
                            <!--<p>新下载
                                <span class="layuiadmin-span-color">10%
                                    <i class="layui-inline layui-icon layui-icon-face-smile-b"></i></span>
                            </p>-->
                        </div>
                    </div>
                </div>
                <div class="layui-col-sm6 layui-col-md3">
                    <div class="layui-card">
                        <div class="layui-card-header">错题数
                            <span class="layui-badge layui-bg-cyan layuiadmin-badge">总计</span></div>
                        <div class="layui-card-body ">
                            <p class="layuiadmin-big-font">33,555</p>
                            <!-- <p>新下载
                                <span class="layuiadmin-span-color">10%
                                    <i class="layui-inline layui-icon layui-icon-face-smile-b"></i></span>
                            </p>-->
                        </div>
                    </div>
                </div>
                <div class="layui-col-sm6 layui-col-md3">
                    <div class="layui-card">
                        <div class="layui-card-header">错题率
                            <span class="layui-badge layui-bg-cyan layuiadmin-badge">百分比</span></div>
                        <div class="layui-card-body ">
                            <p class="layuiadmin-big-font">33,555</p>
                            <!--<p>新下载
                                <span class="layuiadmin-span-color">10%
                                    <i class="layui-inline layui-icon layui-icon-face-smile-b"></i></span>
                            </p>-->
                        </div>
                    </div>
                </div>
                <div class="layui-col-sm6 layui-col-md3">
                    <div class="layui-card">
                        <div class="layui-card-header">做题数
                            <span class="layui-badge layui-bg-cyan layuiadmin-badge">总计</span></div>
                        <div class="layui-card-body ">
                            <p class="layuiadmin-big-font">33555</p>
                            <p>新下载
                                <span class="layuiadmin-span-color">10%
                                    <i class="layui-inline layui-icon layui-icon-face-smile-b"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-header">系统信息</div>
                        <div class="layui-card-body ">
                            <table class="layui-table">
                                <tbody>
                                    <tr>
                                        <th>高理题库版本</th>
                                        <td>2.2</td></tr>
                                    <tr>
                                        <th>服务器地址</th>
                                        <td><?php echo @get_current_user();?> - <?php echo $_SERVER['SERVER_NAME'];?>(<?php if('/'==DIRECTORY_SEPARATOR){echo $_SERVER['SERVER_ADDR'];}else{echo @gethostbyname($_SERVER['SERVER_NAME']);} ?>)&nbsp;&nbsp;你的IP地址是：<?php echo @$_SERVER['REMOTE_ADDR'];?></td></tr>
                                    <tr>
                                        <th>操作系统</th>
                                        <td><?php $os = explode(" ", php_uname()); echo $os[0];?> &nbsp;内核版本：<?php if('/'==DIRECTORY_SEPARATOR){echo $os[2];}else{echo $os[1];} ?></td></tr>
                                    <tr>
                                        <th>运行环境</th>
                                        <td><?php echo $_SERVER['SERVER_SOFTWARE'];?></td></tr>
                                    <tr>
                                        <th>PHP版本</th>
                                        <td><?php echo PHP_VERSION;?></td></tr>
                                    <tr>
                                        <th>PHP运行方式</th>
                                        <td><?php echo strtoupper(php_sapi_name());?></td></tr>
                                    <tr>
                                        <th>MYSQL版本</th>
                                        <td>7.2</td></tr>
                                    <tr>
                                        <th>ThinkPHP</th>
                                        <td>5.0.18</td></tr>
                                    <tr>
                                        <th>上传附件限制</th>
                                        <td></td></tr>
                                    <tr>
                                        <th>执行时间限制</th>
                                        <td></td></tr>
                                    <tr>
                                        <th>剩余空间</th>
                                        <td></td></tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-header">开发</div>
                        <div class="layui-card-body ">
                            <table class="layui-table">
                                <tbody>
                                    <tr>
                                        <th>x-admin开发者</th>
                                        <td>马志斌(113664000@qq.com)</td>
                                    </tr>
                                    <tr>
                                        <th>PHP修改</th>
                                        <td>ZHF</td></tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <style id="welcome_style"></style>
                <div class="layui-col-md12">
                    <blockquote class="layui-elem-quote layui-quote-nm">感谢layui,百度Echarts,jquery,本系统由x-admin提供技术支持。</blockquote></div>
            </div>
        </div>
        </div>
    </body>
</html>